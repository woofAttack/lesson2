using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public float StateButton { get; private set; }


    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        StateButton = 1f;
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        StateButton = 0f;
    }
}
