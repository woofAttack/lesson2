using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputPC : PlayerInput
{
    private PlayerInputActions _inputActions;

    private void Awake()
    {
        _inputActions = new PlayerInputActions();
    }

    private void OnEnable()
    {
        _inputActions.Enable();
    }
    private void OnDisable()
    {
        _inputActions.Disable();
    }

    private protected override float GetBrake()
    {
        return _inputActions.Kart.Brake.ReadValue<float>() + 1f;
    }
    private protected override float GetBoost()
    {
        return _inputActions.Kart.Boost.ReadValue<float>();
    }
    private protected override Vector2 GetMoveDirection()
    {
        return _inputActions.Kart.Move.ReadValue<Vector2>();
    }
}
