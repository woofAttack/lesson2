﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInputJoystick : PlayerInput
{
    [SerializeField] private Joystick _joystickInput;
    [SerializeField] private ButtonListener _buttonBoost;

    private void OnEnable()
    {
        SetActiveVirtualJoystick(true);
    }
    private void OnDisable()
    {
        SetActiveVirtualJoystick(false);
    }

    private void SetActiveVirtualJoystick(bool value)
    {
        _joystickInput.gameObject.SetActive(value);
        _buttonBoost.gameObject.SetActive(value);
    }

    private protected override float GetBoost()
    {
        var value = _buttonBoost.StateButton;
        Debug.Log(value);

        return value;
    }
    private protected override float GetBrake()
    {
        return 1f;
    }
    private protected override Vector2 GetMoveDirection()
    {
        return new Vector2(_joystickInput.Horizontal, _joystickInput.Vertical);
    }
}