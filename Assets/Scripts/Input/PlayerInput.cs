﻿using UnityEngine;

public abstract class PlayerInput : MonoBehaviour
{
    [SerializeField] private Movement _playerMovement;

    private void FixedUpdate()
    {
        var moveDirection = GetMoveDirection();
        var boost = GetBoost();
        var brake = GetBrake();

        Move(moveDirection, brake);
        Rotate(moveDirection);

        Boost(boost);
    }

    private protected abstract float GetBrake();
    private protected abstract float GetBoost();
    private protected abstract Vector2 GetMoveDirection();

    private void Boost(float boost)
    {
        _playerMovement.Boost(boost);
    }
    private void Rotate(Vector2 moveDirection)
    {
        _playerMovement.Rotate(moveDirection);
    }
    private void Move(Vector2 moveDirection, float brake)
    {
        _playerMovement.Move(moveDirection, brake);
    }
}