﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Transform))]
public class Movement : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 20f;
    [SerializeField] private float _rotateSpeed = 90f;
    [SerializeField] private float _addingBoostSpeed = 15f;

    private float _addBoostSpeed;
    private float _deltaMove;

    private Rigidbody _rigidbody;
    private Transform _transform;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = GetComponent<Transform>();
    }

    public void Move(Vector2 direction, float brake)
    {
        /*
        _rigidbody.AddForce(new Vector3(
            x: direction.y * sin * (_moveSpeed + _addBoostSpeed),
            y: 0f,
            z: direction.y * cos * (_moveSpeed + _addBoostSpeed)));
        */

        Func<float> action = () =>
        {
            return direction.y != -1f ? Mathf.Min(direction.y, brake) : direction.y - brake + 1f;
        };

        _deltaMove = Mathf.Lerp(_deltaMove, action.Invoke(), Time.deltaTime * (3f - brake * 2f));

        var angleRad = _transform.rotation.eulerAngles.y * Mathf.Deg2Rad;
        var cos = Mathf.Cos(angleRad);
        var sin = Mathf.Sin(angleRad);

        _rigidbody.velocity = new Vector3(
            x: _deltaMove * sin * (_moveSpeed + _addBoostSpeed),
            y: 0f,
            z: _deltaMove * cos * (_moveSpeed + _addBoostSpeed));
    }
    public void Rotate(Vector2 rotation)
    {
        _transform.Rotate(
            xAngle: 0f,
            yAngle: rotation.x * _rotateSpeed * Time.deltaTime,
            zAngle: 0f);
    }
    public void Boost(float delta)
    {
        _addBoostSpeed = delta * _addingBoostSpeed;
    }
}